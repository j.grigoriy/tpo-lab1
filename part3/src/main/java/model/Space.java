package model;

import java.util.HashSet;
import java.util.Set;

public class Space {
    private final Set<Galaxy> galaxies;

    public Space() {
        galaxies = new HashSet<>();
    }

    public void addGalaxy(Galaxy galaxy) {
        galaxies.add(galaxy);
    }

    public Set<Galaxy> getGalaxies() {
        return galaxies;
    }
}
