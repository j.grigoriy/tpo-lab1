package model;

import exceptions.BlackHoleClosedException;

public class BlackHole {

    private final Galaxy from;
    private final Galaxy to;
    private boolean isClosed;

    public BlackHole(Galaxy from, Galaxy to) {
        this.from = from;
        this.to = to;
        isClosed = false;
    }

    private void open() {
        isClosed = false;
    }

    private void close() {
        isClosed = true;
    }

    public Galaxy getFrom() {
        return from;
    }

    public void transportMessage(String message) {
        if (!isClosed) {
            to.getPhraseFromOtherGalaxy(message);
            close();
        } else {
            throw new BlackHoleClosedException();
        }
    }

    public Galaxy getTo() {
        return to;
    }

    public boolean isClosed() {
        return isClosed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BlackHole blackHole = (BlackHole) o;

        if (isClosed != blackHole.isClosed) return false;
        if (!from.equals(blackHole.from)) return false;
        return to.equals(blackHole.to);
    }

    @Override
    public int hashCode() {
        int result = from.hashCode();
        result = 31 * result + to.hashCode();
        result = 31 * result + (isClosed ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "model.BlackHole{" +
                "from=" + from.toString() +
                ", to=" + to.toString() +
                ", isClosed=" + isClosed +
                '}';
    }
}
