import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BreadthFirstSearchTest {

    private BreadthFirstSearch bfs;
    private GraphNode node0;
    private GraphNode node1;
    private GraphNode node2;
    private GraphNode node3;
    private GraphNode node4;
    private GraphNode node5;
    private GraphNode node6;
    private GraphNode node7;
    private GraphNode node8;
    private GraphNode node9;
    private GraphNode node10;
    private GraphNode node11;
    private GraphNode node12;
    private GraphNode node13;
    private GraphNode node14;
    private GraphNode node15;
    private GraphNode node16;
    private GraphNode node17;

    @BeforeEach
    void initGraphNode() {
        node0 = new GraphNode(0);
        node1 = new GraphNode(1);
        node2 = new GraphNode(2);
        node3 = new GraphNode(3);
        node4 = new GraphNode(4);
        node5 = new GraphNode(5);
        node6 = new GraphNode(6);
        node7 = new GraphNode(7);
        node8 = new GraphNode(8);
        node9 = new GraphNode(9);
        node10 = new GraphNode(10);
        node11 = new GraphNode(11);
        node12 = new GraphNode(12);
        node13 = new GraphNode(13);
        node14 = new GraphNode(14);
        node15 = new GraphNode(15);
        node16 = new GraphNode(16);
        node17 = new GraphNode(17);;
    }

    @Test
    void graphNodeInitTest() {
        node0.addNeighbours(node1, node2, node3);

        Assertions.assertEquals(0, node0.getIndex());
        Assertions.assertEquals(3, node0.getNeighbours().size());
    }

    @Test
    void breadthFirstSearchInitTest() {
        bfs = new BreadthFirstSearch(4, node0);

        Assertions.assertArrayEquals(bfs.getVisited(), new boolean[]{false, false, false, false});
        Assertions.assertArrayEquals(bfs.getParents(), new int[]{-1, -1, -1, -1});
    }

    @Test
    void smallGraphTest() {
        node0.addNeighbours(node1, node2);
        node1.addNeighbours(node0, node2, node6);
        node2.addNeighbours(node0, node1, node4, node5, node6);
        node4.addNeighbours(node2, node6);
        node5.addNeighbours(node2, node7);
        node6.addNeighbours(node1, node2, node4);
        node7.addNeighbours(node5);
        bfs = new BreadthFirstSearch(8, node0);
        bfs.start();

        Assertions.assertArrayEquals(bfs.getVisited(), new boolean[]{true, true, true, false, true, true, true, true});
        Assertions.assertArrayEquals(bfs.getParents(), new int[]{-1, 0, 0, -1, 2, 2, 1, 5});
    }

    @Test
    void largeGraphTest() {
        node0.addNeighbours(node1, node2, node4, node7);
        node1.addNeighbours(node0, node2, node5);
        node2.addNeighbours(node0, node1, node5, node6);
        node3.addNeighbours(node17);
        node4.addNeighbours(node0, node5, node7, node8);
        node5.addNeighbours(node1, node2, node4, node8);
        node6.addNeighbours(node2, node10);
        node7.addNeighbours(node0, node4);
        node8.addNeighbours(node4, node5, node11, node12);
        node9.addNeighbours(node10, node12, node13);
        node10.addNeighbours(node6, node9, node13, node17);
        node11.addNeighbours(node8, node14, node15);
        node12.addNeighbours(node8, node9, node13, node15);
        node13.addNeighbours(node9, node10, node12, node16, node17);
        node14.addNeighbours(node11, node15);
        node15.addNeighbours(node3, node11, node12, node14, node16, node17);
        node16.addNeighbours(node13, node15);
        node17.addNeighbours(node3, node10, node13, node15);
        bfs = new BreadthFirstSearch(18, node10);
        bfs.start();

        Assertions.assertArrayEquals(bfs.getVisited(), new boolean[]{true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true});
        Assertions.assertArrayEquals(bfs.getParents(), new int[]{2, 2, 6, 17, 0, 2, 10, 0, 12, 10, -1, 15, 9, 10, 15, 17, 13, 10});
    }

    @Test
    void noWaysFromNodeTest() {
        node0.addNeighbours(node4);
        node1.addNeighbours(node6);
        node4.addNeighbours(node0, node7);
        node5.addNeighbours(node6, node3);
        node6.addNeighbours(node1, node5);
        node7.addNeighbours(node4);
        bfs = new BreadthFirstSearch(8, node2);
        bfs.start();
        
        Assertions.assertArrayEquals(bfs.getVisited(), new boolean[]{false, false, true, false, false, false, false, false});
        Assertions.assertArrayEquals(bfs.getParents(), new int[]{-1, -1, -1, -1, -1, -1, -1, -1});
    }
}