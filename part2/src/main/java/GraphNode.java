import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GraphNode {

    private final int index;
    private final List<GraphNode> neighbours;

    public GraphNode(int index) {
        this.index = index;
        neighbours = new ArrayList<>();
    }

    public List<GraphNode> getNeighbours() {
        return neighbours;
    }

    public int getIndex() {
        return index;
    }

    public void addNeighbours(GraphNode... neighbours) {
        this.neighbours.addAll(Arrays.asList(neighbours));
    }
}
