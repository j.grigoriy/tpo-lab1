import exceptions.BlackHoleClosedException;
import exceptions.NoSuitableBlackHoleException;
import model.BlackHole;
import model.Galaxy;
import model.Person;
import model.Space;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ModelTests {

    private Space space;
    private Galaxy galaxy1;
    private Galaxy galaxy2;
    private BlackHole blackHole;

    @BeforeEach
    void initSpaceGalaxiesAndBlackHoles() {
        space = new Space();
        galaxy1 = new Galaxy("Млечный путь");
        galaxy2 = new Galaxy("Галлактика Андромеды");
        blackHole = new BlackHole(galaxy1, galaxy2);
    }

    @Test
    void spaceAddGalaxiesTest() {
        space.addGalaxy(galaxy1);
        space.addGalaxy(galaxy2);
        Assertions.assertEquals(2, space.getGalaxies().size());
    }

    @Test
    void spaceNoGalaxiesTest() {
        Assertions.assertEquals(0, space.getGalaxies().size());
    }

    @Test
    void galaxyAddBlackHoleTest() {
        galaxy1.addBlackHole(blackHole);
        Assertions.assertEquals(1, galaxy1.getBlackHoles().size());
    }

    @Test
    void galaxyNoBlackHoleTest() {
        Assertions.assertEquals(0, galaxy1.getBlackHoles().size());
    }

    @Test
    void sendPhraseOverBlackHoleTest() {
        galaxy1.addBlackHole(blackHole);
        galaxy1.sendPhraseToOtherGalaxy("SE.IFMO.RU");
        Assertions.assertTrue(blackHole.isClosed());
        Assertions.assertEquals("SE.IFMO.RU", galaxy2.getLastPhrase());
    }

    @Test
    void sendPhraseOverClosedBlackHoleTest() {
        galaxy1.addBlackHole(blackHole);
        galaxy1.sendPhraseToOtherGalaxy("SE.IFMO.RU");
        Assertions.assertThrows(BlackHoleClosedException.class, () -> galaxy1.sendPhraseToOtherGalaxy("KRUTA"));
    }

    @Test
    void sendPhraseWithoutBlackHoleInGalaxyTest() {
        Assertions.assertThrows(NoSuitableBlackHoleException.class, () -> galaxy1.sendPhraseToOtherGalaxy("KRUTA"));
    }

    @Test
    void personSaysPhraseToOtherGalaxyTest() {
        Person person = new Person("Arthur", galaxy1);
        galaxy1.addBlackHole(blackHole);

        person.sayPhrase("SE.IFMO.RU");

        Assertions.assertTrue(blackHole.isClosed());
        Assertions.assertEquals("SE.IFMO.RU", galaxy2.getLastPhrase());
    }
}
