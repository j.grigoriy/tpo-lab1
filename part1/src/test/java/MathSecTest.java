import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

public class MathSecTest {
    private static final double DELTA = 0.001;

    @ParameterizedTest
    @CsvFileSource(resources = "/sec_test_data.csv")
    void secFunctionParameterizedTest(double input, double expectedOutput) {
        double givenOutput = MathSec.sec(input);
        Assertions.assertEquals(expectedOutput, givenOutput, DELTA);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sec_throw_data.csv")
    void secFunctionThrowsTest(double input) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> MathSec.sec(input));
    }

    @Test
    void testPositiveInfinity() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> MathSec.sec(Double.POSITIVE_INFINITY));
    }

    @Test
    void testNegativeInfinity() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> MathSec.sec(Double.NEGATIVE_INFINITY));
    }

    @Test
    void testNaN() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> MathSec.sec(Double.NaN));
    }
}
