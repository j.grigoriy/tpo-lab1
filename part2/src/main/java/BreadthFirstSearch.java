import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

public class BreadthFirstSearch {
    private final boolean[] visited;
    private final int[] parents;
    private final GraphNode root;

    public BreadthFirstSearch(int elementsCount, GraphNode root) {
        this.root = root;

        visited = new boolean[elementsCount];
        parents = new int[elementsCount];

        Arrays.fill(parents, -1);
        Arrays.fill(visited, false);
    }

    public void start() {
        Queue<GraphNode> graphNodes = new ArrayDeque<>();
        graphNodes.add(root);
        visited[root.getIndex()] = true;
        while (!graphNodes.isEmpty()) {
            GraphNode current = graphNodes.remove();
            for (GraphNode neighbour : current.getNeighbours()) {
                if (!visited[neighbour.getIndex()]) {
                    parents[neighbour.getIndex()] = current.getIndex();
                    visited[neighbour.getIndex()] = true;
                    graphNodes.add(neighbour);
                }
            }
        }
    }

    public boolean[] getVisited() {
        return visited;
    }

    public int[] getParents() {
        return parents;
    }
}
