package model;

import exceptions.NoSuitableBlackHoleException;

import java.util.*;

public class Galaxy {

    private final String name;
    private String lastPhrase;
    private final List<BlackHole> blackHoles;

    public Galaxy(String name) {
        this.name = name;
        lastPhrase = null;
        blackHoles = new ArrayList<>();
    }

    public void sendPhraseToOtherGalaxy(String phrase) {
        Random random = new Random();
        if (blackHoles.size() != 0) {
            blackHoles.get(random.nextInt(blackHoles.size())).transportMessage(phrase);
        } else {
            throw new NoSuitableBlackHoleException();
        }
    }

    public void getPhraseFromOtherGalaxy(String phrase) {
        this.lastPhrase = phrase;
    }

    public String getName() {
        return name;
    }

    public String getLastPhrase() {
        return lastPhrase;
    }

    public List<BlackHole> getBlackHoles() {
        return blackHoles;
    }

    public void addBlackHole(BlackHole blackHole) {
        blackHoles.add(blackHole);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Galaxy galaxy = (Galaxy) o;

        if (!name.equals(galaxy.name)) return false;
        return lastPhrase != null ? lastPhrase.equals(galaxy.lastPhrase) : galaxy.lastPhrase == null;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + (lastPhrase != null ? lastPhrase.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "model.Galaxy{" +
                "name='" + name + '\'' +
                ", lastPhrase='" + lastPhrase + '\'' +
                '}';
    }
}
