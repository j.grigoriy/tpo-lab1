package model;

public abstract class Creature {
    abstract public void sayPhrase(String phrase);
    abstract public String toString();
}
