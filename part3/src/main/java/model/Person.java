package model;

public class Person extends Creature {

    private final String name;
    private Galaxy currentGalaxy;

    public Person(String name, Galaxy currentGalaxy) {
        this.name = name;
        this.currentGalaxy = currentGalaxy;
    }

    public String getName() {
        return name;
    }

    public Galaxy getCurrentGalaxy() {
        return currentGalaxy;
    }

    public void setCurrentGalaxy(Galaxy currentGalaxy) {
        this.currentGalaxy = currentGalaxy;
    }

    @Override
    public void sayPhrase(String phrase) {
        currentGalaxy.sendPhraseToOtherGalaxy(phrase);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (!name.equals(person.name)) return false;
        return currentGalaxy.equals(person.currentGalaxy);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + currentGalaxy.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "model.Person{" +
                "name='" + name + '\'' +
                '}';
    }
}
