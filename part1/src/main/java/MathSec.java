public class MathSec {
    public static double sec(double x) {

        if (x == Double.POSITIVE_INFINITY ||
                x == Double.NEGATIVE_INFINITY ||
                Double.isNaN(x)) {
            throw new IllegalArgumentException("Invalid input");
        }

        if (x > 0) {
            while (x >= 3*Math.PI/2) {
                x -= Math.PI * 2;
            }
        } else if (x < 0) {
            while (x <= -Math.PI/2) {
                x += Math.PI * 2;
            }
        }

        if (x == -Math.PI/2 || x == Math.PI/2 || x == 3*Math.PI/2) {
            throw new IllegalArgumentException("Invalid input");
        }

        double result = 1;
        short sign = 1;
        long factorial = 1;
        double xPower = 1;
        for (int n = 1; n < 11; n++) {
            xPower *= x * x;
            factorial *= (n * 2 - 1) * (n * 2);
            sign *= -1;
            result += sign * (xPower / factorial);
        }

        return 1 / result;
    }
}
